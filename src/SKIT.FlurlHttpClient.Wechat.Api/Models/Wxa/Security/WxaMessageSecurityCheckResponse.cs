﻿using System;
using System.Collections.Generic;

namespace SKIT.FlurlHttpClient.Wechat.Api.Models
{
    /// <summary>
    /// <para>表示 [POST] /wxa/msg_sec_check 接口的响应。</para>
    /// </summary>
    public class WxaMessageSecurityCheckResponse : WechatApiResponse
    {
    }
}
